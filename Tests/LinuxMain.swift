import XCTest

import StatisticsTests

var tests = [XCTestCaseEntry]()
tests += StatisticsTests.allTests()
tests += RollingAverageTests.allTests()
XCTMain(tests)
