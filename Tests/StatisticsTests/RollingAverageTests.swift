//
//  RollingAverageTests.swift
//  
//
//  Created by Jeremy Pereira on 22/05/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
import Statistics

private let log = Logger.getLogger("StatisticsTests.RollingAverageTests")

class RollingAverageTests: XCTestCase
{
    static var allTests = [
        ("testRollingAverage", testRollingAverage),
		("testNonMutatingAdd", testNonMutatingAdd),
    ]

	func testRollingAverage()
	{
		var rollingAverage = RollingAverage<Double>(period: 8)
		XCTAssert(rollingAverage.sum == 0)
		XCTAssert(rollingAverage.mean == nil)
		rollingAverage.add(value: 2)
		XCTAssert(rollingAverage.sum == 2)
		XCTAssert(rollingAverage.mean == 2)
		for i in 0 ..< 8
		{
			rollingAverage.add(value: 0)
			XCTAssert((rollingAverage.sum == 2 && i < 7) || (rollingAverage.sum == 0 && i == 7),
					  "Incorrect sum: i = \(i), sum = \(rollingAverage.sum)")
			guard let mean = rollingAverage.mean
				else { XCTFail("Got no mean") ; break }
			XCTAssert((i < 7 && mean == 2 / Double(i + 2)) || (i == 7 && mean == 0),
					  "Incorrect mean, got i = \(i), mean = \(mean)")
		}
	}

	func testNonMutatingAdd()
	{
		let rollingAverage = RollingAverage<Double>(period: 8)
		XCTAssert(rollingAverage.sum == 0)
		XCTAssert(rollingAverage.mean == nil)
		let ra2 = rollingAverage.adding(value: 2)
		XCTAssert(rollingAverage.sum == 0)
		XCTAssert(rollingAverage.mean == nil)
		XCTAssert(ra2.sum == 2)
		XCTAssert(ra2.mean == 2)
	}

}
