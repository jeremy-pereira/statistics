//
//  StatisticsTests.swift
//  NoughtsAndCrossesTests
//
//  Created by Jeremy Pereira on 15/02/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
import SQL
import Statistics

private let log = Logger.getLogger("StatisticsTests.StatisticsTests")

fileprivate struct TestStats: Recordable
{
	var data: [String : StatsMappable]
	{
		[
			"count" : count,
			"total" : total,
			"average" : average
		]
	}

	static var dataSetName = "foo"

	var count: Int
	var total: Int
	var average: Double
}

fileprivate struct TestStatsWithNil: Recordable
{
	var data: [String : StatsMappable]
	{
		[
			"count" : count,
			"total" : total,
			"average" : average
		]
	}

	static var dataSetName = "foo"

	var count: Int
	var total: Int
	var average: Double?
}

fileprivate struct PivotData: Recordable
{
	static let dataSetName = "pivot"

	init(row: String, col: String, value: Int)
	{
		data = ["rowName" : row,
				"colName": col,
				"value" : value]
	}

	var data: [String : StatsMappable]
}

class StatisticsTests: XCTestCase
{
	let createFromScratchDb = "createFromScratch.sqlite3"
	let resultsDb = "results.sqlite3"
	let fm = FileManager.default

    override func setUp()
	{
		log.pushLevel(.info)
		log.info("Where am I? \(fm.currentDirectoryPath)")
		// Delete the DB that is not supposed to exist
		deleteFile(name: createFromScratchDb)
		deleteFile(name: resultsDb)
    }

	private func deleteFile(name: String)
	{
        var dbBytes = try! name.signedBytes(encoding: String.Encoding.utf8)
        dbBytes.append(0)
        unlink(&dbBytes)
	}

    override func tearDown()
	{
		log.popLevel()
    }

    func testCreateFromScratch()
	{
		do
		{
			let stats = try Statistics<TestStats>(sqlite3File: createFromScratchDb)
			log.info("\(#function): Created \(fm.currentDirectoryPath)/\(createFromScratchDb)")
			XCTAssert(stats.url == URL(fileURLWithPath: fm.currentDirectoryPath + "/" + createFromScratchDb), "Incorrect URL: '\(stats.url)'")
			let dataPoint = TestStats(count: 1, total: 2, average: 0.5)
			try stats.record(dataPoint: dataPoint)
			try stats.record(dataPoint: dataPoint)
			let countSelect = Statement.select(columnNames: ["count(*)"], from: TableName(TestStats.dataSetName))
			try stats.connection.with(statement: countSelect)
			{
				try $0.execute()
				let results = $0.resultSet
				guard let firstResult = try results.next()
					else { XCTFail("No count result ") ; return }
				guard let count = firstResult["count(*)"]
					else { XCTFail("count not called count") ; return }
				XCTAssert(count.asInt == 2, "Wrong count \(count)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

    func testClear()
	{
		do
		{
			let stats = try Statistics<TestStats>(sqlite3File: createFromScratchDb)
			log.info("\(#function): Created \(fm.currentDirectoryPath)/\(createFromScratchDb)")
			XCTAssert(stats.url == URL(fileURLWithPath: fm.currentDirectoryPath + "/" + createFromScratchDb), "Incorrect URL: '\(stats.url)'")
			let dataPoint = TestStats(count: 1, total: 2, average: 0.5)
			try stats.record(dataPoint: dataPoint)
			try stats.record(dataPoint: dataPoint)
			try stats.clear()
			let countSelect = Statement.select(columnNames: ["count(*)"], from: TableName(TestStats.dataSetName))
			try stats.connection.with(statement: countSelect)
			{
				try $0.execute()
				let results = $0.resultSet
				guard let firstResult = try results.next()
					else { XCTFail("No count result ") ; return }
				guard let count = firstResult["count(*)"]
					else { XCTFail("count not called count") ; return }
				XCTAssert(count.asInt == 0, "Wrong count \(count)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

    func testClearPreexisting()
	{
		do
		{
			let stats = try Statistics<TestStats>(sqlite3File: createFromScratchDb)
			log.info("\(#function): Created \(fm.currentDirectoryPath)/\(createFromScratchDb)")
			XCTAssert(stats.url == URL(fileURLWithPath: fm.currentDirectoryPath + "/" + createFromScratchDb), "Incorrect URL: '\(stats.url)'")
			let dataPoint = TestStats(count: 1, total: 2, average: 0.5)
			try stats.record(dataPoint: dataPoint)
			try stats.record(dataPoint: dataPoint)
			try stats.clear()
			let countSelect = Statement.select(columnNames: ["count(*)"], from: TableName(TestStats.dataSetName))
			try stats.connection.with(statement: countSelect)
			{
				try $0.execute()
				let results = $0.resultSet
				guard let firstResult = try results.next()
					else { XCTFail("No count result ") ; return }
				guard let count = firstResult["count(*)"]
					else { XCTFail("count not called count") ; return }
				XCTAssert(count.asInt == 0, "Wrong count \(count)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
    }


    func testOverrideDataSetName()
	{
		do
		{
			let stats = try Statistics<TestStats>(sqlite3File: createFromScratchDb, dataSetName: "MyData")
			log.info("\(#function): Created \(fm.currentDirectoryPath)/\(createFromScratchDb)")
			XCTAssert(stats.url == URL(fileURLWithPath: fm.currentDirectoryPath + "/" + createFromScratchDb), "Incorrect URL: '\(stats.url)'")
			let dataPoint = TestStats(count: 1, total: 2, average: 0.5)
			try stats.record(dataPoint: dataPoint)
			try stats.record(dataPoint: dataPoint)
			let countSelect = Statement.select(columnNames: ["count(*)"], from: TableName("MyData"))
			try stats.connection.with(statement: countSelect)
			{
				try $0.execute()
				let results = $0.resultSet
				guard let firstResult = try results.next()
					else { XCTFail("No count result ") ; return }
				guard let count = firstResult["count(*)"]
					else { XCTFail("count not called count") ; return }
				XCTAssert(count.asInt == 2, "Wrong count \(count)")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

	func testPivot()
	{
		do
		{
			let stats = try Statistics<PivotData>(sqlite3File: resultsDb)
			log.info("\(#function): Created \(stats.url)")
			let d1 = PivotData(row: "1", col: "a", value: 1)
			let d2 = PivotData(row: "2", col: "a", value: 3)
			try stats.record(dataPoint: d1)
			try stats.record(dataPoint: d1)
			try stats.record(dataPoint: d2)
			let pivot = stats.pivot(rowField: "rowName", columnField: "colName", cell: [("value", Statistics<PivotData>.Pivot.Aggregator.sum)])
			var rows = 0
			try pivot.forEach
			{
				row, col, values in
				log.debug("\(row). \(col), \(values)")
				switch row.asString!
				{
				case "1":
					XCTAssert(values[0].asInt! == 2)
				case "2":
					XCTAssert(values[0].asInt! == 3)
				default:
					XCTFail("Expected row to be 1 or 2, not \(row)")
				}
				rows += 1
			}
			XCTAssert(rows == 2)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testTag()
	{
		do
		{
			let stats = try Statistics<PivotData>(sqlite3File: resultsDb)
			log.info("\(#function): Created \(stats.url)")
			let d1 = PivotData(row: "1", col: "a", value: 1)
			try stats.record(dataPoint: d1)
			XCTAssert(try stats.latestSequenceNumber() == 1)
			try stats.record(dataPoint: d1)
			XCTAssert(try stats.latestSequenceNumber() == 2)
			try stats.record(dataPoint: d1)
			try stats.tag(tag: "Should be sequence 3")
			try stats.tag(sequenceNumber: 1, tag: "Should be sequence 1")
			try stats.record(dataPoint: d1, tag: "Should be sequence 4")
			let select = Statement.select(columnNames: ["id", "tableName", "sequenceNumber", "tag"],
										  from: TableName("tags"))
			let tagCount = try stats.connection.with(statement: select)
			{
				(p) -> Int in
				try p.execute()
				let resultSet = p.resultSet
				var count = 0
				while let result = try resultSet.next()
				{
					count += 1
					if let sequenceNumber = result["sequenceNumber"]?.asInt
					{
						if let tag = result["tag"]?.asString
						{
							switch sequenceNumber
							{
							case 1:
								XCTAssert(tag == "Should be sequence 1")
							case 3:
								XCTAssert(tag == "Should be sequence 3")
							case 4:
								XCTAssert(tag == "Should be sequence 4")
							default:
								XCTFail("Wrong sequence number: \(sequenceNumber)")
							}
						}
						else
						{
							XCTFail("Could not get tag from \(result)")
						}
					}
					else
					{
						XCTFail("Could not get sequence number from result \(result)")
					}
				}
				return count
			}
			XCTAssert(tagCount == 3, "Wrong  tag count: \(tagCount)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testOptionalDouble()
	{
		do
		{
			let stats = try Statistics<TestStatsWithNil>(sqlite3File: createFromScratchDb)
			log.info("\(#function): Created \(fm.currentDirectoryPath)/\(createFromScratchDb)")
			XCTAssert(stats.url == URL(fileURLWithPath: fm.currentDirectoryPath + "/" + createFromScratchDb), "Incorrect URL: '\(stats.url)'")
			let dataPoint1 = TestStatsWithNil(count: 0, total: 2, average: nil)
			let dataPoint2 = TestStatsWithNil(count: 1, total: 2, average: 0.5)
			try stats.record(dataPoint: dataPoint1)
			try stats.record(dataPoint: dataPoint2)
			let countSelect = Statement.select(columnNames: ["average"], from: TableName(TestStats.dataSetName))
			try stats.connection.with(statement: countSelect)
			{
				try $0.execute()
				let results = $0.resultSet
				guard let firstResult = try results.next()
					else { XCTFail("No average result ") ; return }
				guard case .null = firstResult["average"]
					else { XCTFail("average shuld be null") ; return }
				guard let secondResult = try results.next()
					else { XCTFail("No second result ") ; return }
				guard case .float(let average) = secondResult["average"]
					else { XCTFail("No saverage") ; return }
				XCTAssert(average == 0.5)
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}
}
