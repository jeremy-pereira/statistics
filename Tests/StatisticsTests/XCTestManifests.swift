import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(StatisticsTests.allTests),
		testCase(RollingAverageTests.allTests),
    ]
}
#endif
