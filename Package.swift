// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Statistics",
    platforms: [
        .macOS(.v10_15),
    ],
products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "Statistics",
            targets: ["Statistics"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
		.package(name: "SQL", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/sql.git", from: "5.0.0"),
		.package(name: "Toolbox", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/Toolbox.git", from: "21.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "Statistics",
            dependencies: ["SQL", "Toolbox"]),
        .testTarget(
            name: "StatisticsTests",
            dependencies: ["Statistics", "SQL", "Toolbox"]),
    ]
)
