//
//  StatsMappable.swift
//  
//
//  Created by Jeremy Pereira on 17/05/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import SQL


/// Protocol that must be adopted by any type used as a value in a datapoint
///
/// It is necessary to adopt this type so that this module can map values to
/// values for SQL insert statements and also to determine the type so the
/// stats table can be constructed.
///
/// Implementations exist for some Swift types like `String` and `Int`.
public protocol StatsMappable
{
	/// The value of an instance mapped to an SQL type
	var sqlValue: SQLValue { get }
	/// The SQL type of an instance.
	///
	static var sqlType: SQLType { get }
}

extension StatsMappable
{
	var sqlType: SQLType { Self.sqlType }
}

extension Int: StatsMappable
{
	public static var sqlType: SQLType { .integer }

	public var sqlValue: SQLValue { .integer(self) }
}

extension Double: StatsMappable
{
	public var sqlValue: SQLValue { .float(self) }

	public static var sqlType: SQLType { .float }
}

extension String: StatsMappable
{
	public var sqlValue: SQLValue { .varChar(self) }

	public static var sqlType: SQLType { .text }
}

extension Optional: StatsMappable where Wrapped: StatsMappable
{
	public var sqlValue: SQLValue
	{
		switch self
		{
		case .none:
			return .null
		case .some(let value):
			return value.sqlValue
		}
	}
	public static var sqlType: SQLType { Wrapped.sqlType }
}

extension Date: StatsMappable
{
	public static var sqlType: SQLType { .date }

	public var sqlValue: SQLValue { .date(self) }
}
