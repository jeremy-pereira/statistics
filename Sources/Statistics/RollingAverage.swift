//
//  RollingAverage.swift
//  
//
//  Created by Jeremy Pereira on 22/05/2020.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// Protocol that must be conformed to by any type that can have a mean
/// average taken.
///
/// It must be possible to add two values of the type together, so we can
/// obtain a sum. It must also be possible to divide a vlue by an `Int` so we
/// can divide the sum by the number of samples we have.
///
/// To make it easier, a default implementation of `dividedBy(int: Int)` is
/// provided for any `Averagable` that also conforms to `FloatingPoint` so all
/// that is needed is to declare conformance. `Double` and `Float` already
/// conform.
public protocol Averagable
{
	/// You must be able to add values together
	/// - Parameters:
	///   - lhs: A value to addd
	///   - rhs: Another value to add
	/// - Returns: the sum of two values
	static func +(lhs: Self, rhs: Self) -> Self

	/// You must be able to divide a value by an integer value
	///
	/// This is so we can divide the sum of values by the number of samples.
	/// There is a default implementation for floating point types.
	/// - Parameter int: divisor
	/// - Returns: the result of dividing the value by an integer qhanitity.
	func dividedBy(int: Int) -> Self

	/// Zero for this type
	///
	///  i.e. identity under addition.
	static var zero: Self { get }
}

public extension Averagable where Self: FloatingPoint
{
	func dividedBy(int: Int) -> Self
	{
		return self / Self(int)
	}
}

extension Double: Averagable {}
extension Float: Averagable {}

/// A type that gives a rolling average over a specified period.
///
/// You can take a rolling average over values of any type that conforms to
/// `Averagable` which means any type where values can be summed and where a sum
/// can be divided by an integer. `Double` and `Float` already conform to
/// `Averagable` and any other numeric type can be made to conform by implementing
/// `dividedBy(int: Int)`. Note that a default implementation exists for any
/// conforming type that also conforms to `FloatingPoint`.
public struct RollingAverage<N: Averagable>
{
	public let period: Int

	private var values: [N] = []
	private var addIndex: Int = 0

	public init(period: Int)
	{
		self.period = period
	}

	/// The current rolling average
	///
	///This is `nil` if no values have yet been recorded.
	///
	/// Note that this will have the generic type `N?` which means that for
	/// integers you'll lose the decimal part.
	public var mean: N?
	{
		guard values.count > 0 else { return nil }
		return sum.dividedBy(int: values.count)
	}

	/// The sum of the values recorded
	public var sum: N { values.reduce(N.zero, +) }

	/// Add a new value to the rolling aerage.
	///
	/// If the number of values thereby exceeds the period, the oldest value is
	/// discarded.
	/// - Parameter value: A value to add
	public mutating func add(value: N)
	{
		if values.count < period
		{
			values.append(value)
		}
		else
		{
			values[addIndex] = value
			addIndex = (addIndex + 1) % period
		}
	}

	/// Returns a new rolling average adding the value to this one
	/// - Parameter value: The value to add
	/// - Returns: A new rolling average with a new value and discarding this
	///            objects last value.
	public func adding(value: N) -> RollingAverage<N>
	{
		var ret = self
		ret.add(value: value)
		return ret
	}
}
