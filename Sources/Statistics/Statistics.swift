//
//  Statistics.swift
//  NoughtsAndCrosses
//
//  Created by Jeremy Pereira on 14/02/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import SQL
import Toolbox

fileprivate let log = Logger.getLogger("Statistics.Statistics")

/// Protocol to be implemented by any type that models a datapoint to be collected
public protocol Recordable
{
	/// The default name of the dataset to put the datapoints in
	///
	/// The name must follow the conventions for naming a table in SQLite3.
	///
	/// The default name can be overriden at the point where the `Statistics`
	/// object is created. Also, a default implementation is provided where the
	/// data set name is the type name.
	static var dataSetName: String { get }
	/// The data associated with this data point
	var data: [String : StatsMappable] { get }
}

public extension Recordable
{
	static var dataSetName: String { "\(Self.self)" }
}

/// Type for managing recorded statistics
///
/// This is a class because it has to manage a database connection.
///
/// A Statistics is responsible for recording one kind of data point. If you
/// want to put more than one kind of statistic in the same file, you can but
/// need to open multiple connections to the underlying database at this point.
///
/// - Todo: Create a `Statistics` from an existing database connection.
open class Statistics<T: Recordable>
{
	/// The database connection for this statistics object
	public private(set) var connection: Connection

	private static var tagsTableName: TableName { TableName("tags") }
	private static var tagsCreate: Statement
	{
		Statement.create(table: tagsTableName,
						 columns: [Column(name: "id",
										  sqlType: .integer,
										  constraints: [.primaryKey(autoIncrement: true)]),
								   Column(name: "tableName", sqlType: .text, constraints: []),
								   Column(name: "sequenceNumber", sqlType: .integer, constraints: []),
								   Column(name: "tag", sqlType: .text, constraints: [])],
						 constraints: [])
	}

	/// Create a Statistics object
	/// - Parameters:
	///   - sqlite3File: The file name of the SQLite3 database
	///   - dataSetName: The name of the data set. If `nil` it defaults to the
	///                  `T.dataSetName`. The same lexicographic rules apply to
	///                  this as to table names in SQLite3.
	/// - Throws: If the database connection can't be opened.
	public init(sqlite3File: String, dataSetName: String? = nil) throws
	{
		url = URL.init(fileURLWithPath: sqlite3File).absoluteURL
		let connection = SQLLite3Connection(fileName: sqlite3File)
		try connection.open()
		self.connection = connection
		self.dataSetName = dataSetName ?? T.dataSetName
	}

	deinit
	{
		connection.close()
	}

	private var tablesDefinitelyExist = false

	private func makeTable(sampleData: [String : StatsMappable]) throws
	{
		// First, don't do anything if we created the tables
		guard !tablesDefinitelyExist else { return }

		// Make the table
		if try !connection.has(table: TableName(dataSetName))
		{
			try connection.with(statement: makeCreateStatement(from: sampleData))
			{
				try $0.execute()
			}
		}
		// Make the tags table
		if try !connection.has(table: Statistics.tagsTableName)
		{
			try connection.with(statement: Statistics.tagsCreate) { try $0.execute() }
		}
		// Assert that we have definitely created the tables
		tablesDefinitelyExist = true
	}

	/// The name of the dataset
	///
	/// This is the name of the table in SQLite used to hold data points for the
	/// data set.
	public let dataSetName: String

	/// Location of the underlying SQLite file
	public let url: URL

	/// Record a datapoint
	///
	/// - todo: There is a possibility of an SQL injection attack based on the
	///         column names. Need to fix this.
	/// - Parameter dataPoint: The datapoint to record.
	/// - Throws: if we are unable to create a datapoint in the database.
	open func record(dataPoint: T, tag: String? = nil) throws
	{
		guard !isFinished else { throw Error.finishedCollectingStats }

		let data = dataPoint.data
		// The first datapoint in a new data set won't have a table for it.
		try makeTable(sampleData: data)

		let keys = Array(data.keys)
		let table = TableName(dataSetName)
		let insert = Statement.insert(table: table, columnNames: keys)
		try connection.transaction
		{
			try connection.with(statement: insert)
			{
				p in
				let values = keys.map { data[$0]!.sqlValue }
				try p.bind(values: values)
				try p.execute()
			}
			if let tag = tag
			{
				try self.tag(tag: tag)
			}
		}
	}

	/// Clears the existing data out of the dataset
	///
	/// - Throws: if we are unable to delete the data in the table
	open func clear() throws
	{
		guard !isFinished else { throw Error.finishedCollectingStats }
		if try connection.has(table: TableName(dataSetName))
		{
			let statement = Statement.delete(from: TableName(dataSetName), whereClause: nil)
			try connection.with(statement: statement)
			{
				try $0.execute()
			}
		}
	}

	private var sequenceSelect: Statement
	{
		Statement.select(columnNames: ["max(sequenceNumber)"], from: TableName(dataSetName))
	}
	/// The sequence number of the last data point recorded
	///
	/// - Returns: The latest sequence number for a data point in the data set.
	/// - Throws: If the dataset query for the sequence number fails for some
	///           reason.
	open func latestSequenceNumber() throws -> Int
	{
		return try connection.with(statement: sequenceSelect)
		{
			(p) -> Int in
			try p.execute()
			guard let row = try p.resultSet.next() else { throw Error.noResultsFound("\(sequenceSelect)") }
			guard let ret = row["max(sequenceNumber)"] else { fatalError("Column name mismatch") }
			return ret.asInt!
		}
	}

	private func makeCreateStatement(from sampleData: [String : StatsMappable]) -> Statement
	{
		let columnNames = sampleData.keys.sorted()
		let columns = columnNames.map
		{
			(columnName: String) -> (String, SQLType) in
			let sqlType = sampleData[columnName]!.sqlType
			return (columnName, sqlType)
		}
		let columnDefs = columns.map
		{
			(columnDef) -> Column in
			let (name, type) = columnDef
			return Column(name: name, sqlType: type, constraints: [])
		}
		return Statement.create(table: TableName(dataSetName),
								columns: [Column(name: "sequenceNumber",
												 sqlType: .integer,
												 constraints: [.primaryKey(autoIncrement: true)])] + columnDefs,
								constraints: [])
	}

	private static var insertTag: Statement
	{
		Statement.insert(table: tagsTableName, columnNames: ["tableName", "sequenceNumber", "tag"])
	}

	/// Add a tag for a sequence number
	/// - Parameters:
	///   - sequenceNumber: The sequence number, defaults to the latest
	///                     sequence number in the data set.
	///   - tag: The tag to add
	/// - Throws: If the connection is not open, or there is an error storing
	///           the tag.
	public func tag(sequenceNumber: Int? = nil, tag: String) throws
	{
		guard !isFinished else { throw Error.finishedCollectingStats }

		try connection.with(statement: Statistics.insertTag)
		{
			try $0.bind(values: [.varChar(dataSetName),
							     .integer(sequenceNumber ?? self.latestSequenceNumber()),
								 .varChar(tag)])
			try $0.execute()
		}
	}

	private var isFinished = false

	/// Stop collecting statistics
	///
	/// Once this function is used, you can no longer use the instance to collect
	/// statistics. The function is useful to force closed the database
	/// connection when you want to delete its file.
	///
	/// It's safe to call this function on an already finished stats object.
	public func finish()
	{
		guard !isFinished else { return }
		connection.close()
	}
}

extension Statistics
{
	/// Errors associated with using the statistics object
	public enum Error: Swift.Error
	{
		/// The attempt to record a statistic failed because `finish()` has
		/// already been called.
		case finishedCollectingStats
		/// A SQL select statement returned no results when one was expected
		case noResultsFound(String)
	}
}

extension Statistics
{

	/// Create a pivot table from a set of statistics.
	///
	/// This creates a table with a set of rows and columns and values for  the
	/// cell corresponding to each row and column. The row can be any one of
	/// the data set's columns and so can the column. The cell values must be
	/// numeric unless you are using `count`.
	///
	/// - Parameters:
	///   - rowField: The field by which to split the rows
	///   - columnField: The field by which to split the columns
	///   - cell: The cell containing the data, consisting of an array of fields
	///           and an aggregator function for each.
	public func pivot(rowField: String, columnField: String, cell: [(String, Pivot.Aggregator)]) -> Pivot
	{
		Pivot(source: self, rowField: rowField, columnField: columnField, cell: cell)
	}
	/// Create a pivot table from a set of statistics.
	///
	/// This creates a table with a set of rows and columns and values for  the
	/// cell corresponding to each row and column. The row can be any one of
	/// the data set's columns and so can the column. The cell values must be
	/// numeric unless you are using `count`.
	public struct Pivot
	{

		/// Ways to aggregate cell vbalues.
		public enum Aggregator
		{
			/// The count of values
			case count
			/// The sum of values
			case sum
			/// The average of values
			case average
			/// The maximum value
			case max
			/// The minimum of the values
			case min

			fileprivate func makeSQL(_ fieldName: String) -> String
			{
				switch self
				{

				case .count:
					return "COUNT(\(fieldName))"
				case .sum:
					return "SUM(\(fieldName))"
				case .average:
					return "AVG(\(fieldName))"
				case .max:
					return "MAX(\(fieldName))"
				case .min:
					return "MIN(\(fieldName))"
				}
			}
		}

		private var source: Statistics
		private var select: Statement
		private var rowField: String
		private var columnField: String
		private var cellNames: [String]

		fileprivate init(source: Statistics, rowField: String, columnField: String, cell: [(String, Aggregator)])
		{
			self.source = source
			// We need to construct a select statement. First get the component
			// parts.
			let table = TableName(source.dataSetName)
			let columns = cell.map
			{
				(field) -> String in
				let (name, aggregate) = field
				return aggregate.makeSQL(name)
			}
			select = Statement.select(columnNames: [rowField, columnField] + columns,
										  	 from: table,
										  groupBy: [rowField, columnField])
			// Store these for later retrieval by `forEach`
			self.rowField = rowField
			self.columnField = columnField
			self.cellNames = columns
		}

		/// Iterate through each cell of the pivot table running a block on it
		/// - Parameter block: A closure that processes the cell values
		/// - Throws: if the connection is closed or the select fails.
		public func forEach(_ block: (_ row: SQLValue, _ col: SQLValue, _ values: [SQLValue]) -> ()) throws
		{
			guard !source.isFinished else { throw Statistics.Error.finishedCollectingStats }
			log.debug("Pivot select is \(source.connection.stringFrom(statement: select))")
			try source.connection.with(statement: select)
			{
				try $0.execute()
				while let resultRow = try $0.resultSet.next()
				{
					// We fatal here because it means we set the select incorrectly
					guard let row = resultRow[rowField] else { fatalError("\(rowField) not returned in results \(resultRow.keys)") }
					guard let col = resultRow[columnField] else { fatalError("\(columnField) not returned in results \(resultRow.keys)") }
					let values: [SQLValue] = cellNames.map
					{
						guard let ret = resultRow[$0]
							else { fatalError("cell name \($0) is not in result set \(resultRow.keys)") }
						return ret
					}
					block(row, col, values)
				}
			}
		}
	}
}
