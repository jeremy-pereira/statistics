# Statistics

A package that can record data points to a *sqlite3* database.

This package depends on my SQL package although the gory details of how it is used are hidden from the user of this package, except for the use of the `SQLValue` enumeration for manipulating values in a type safe way.

## Why?

I have written many programs where I wanted to output some statistics. For example, I have a Noughts and Crosses program that can  pit you against a machine learning noughts and crosses player. It would be great, I thought, to output stats so that I could analyse the program's improvement over time. At first, I thought of writing the stats to a text file, but then it occurred to me that, if I wrote it to an *sqlite3* database, I could use SQL to do preliminary processing before importing to an Excel spreadsheet.

This package is my attempt at this goal. 

## Usage

You need a type that represents a data point that conforms to the protocol `Recordable`. This type must implement two items:

- a `static` property that is the name of the data set. Do not call the data set "tags": that name is reserved for the `tags` table (see below). Do not call your data set "table" or any other SQL keyword.

- a "data" property that delivers a dictionary keyed by the value names and values of appropriate `StatsMappable`s. The keys must not be SQL keywords, because, ultimately, they are used as columns in a SQL table.

  `StatsMappable` is a protocol that defines methods for getting a `SQLValue` from a value and a `SQLType` from a type. You can make any type conform to `StatsMappable` by extending it to conform to the protocol. 

Note that an `Optional` that wraps a `StatsMappable` is automatically `StatsMappable` itself. Also, conformances have been defined for `Int`, `Double` and `String` already.

You also need a `Statistics` object with its generic parameter set to your `Recordable` type. When you instantiate it, you give it a file name and it will auomatically create an *sqlite* database at that location with a table schema derrived from the `dataSet` property and the first dictionary of values recorded, or if the file exists, it will try to add a table for your `Recordable` type, or it will use an existing table with the same name. This might not work if it was created by a `Statistics` object with a different `Recordable`. 

Here is a simple implementation:

```Swift
struct PivotData: Recordable
{
	/// The data set has the name 'pivot'
	static let dataSetName = "pivot"

	/// In this case we initialise `PivotData` with the
	/// values we want to record
	init(row: String, col: String, value: Int)
	{
		data = ["rowName" : row,
				"colName": col,
				"value" : value]
	}
	/// the values to record.
	private(set) var data: [String : SQLValue]
}
```
The above defines a `Recordable`. Note that the `Statistics` library contains extensions to `Int` and `String` to make them `StatsMappable`.

Here's how to use it

```Swift
let stats = try Statistics<PivotData>(sqlite3File: "myResults.sqlite")
let d1 = PivotData(row: "1", col: "a", value: 1)
let d2 = PivotData(row: "2", col: "a", value: 3)
try stats.record(dataPoint: d1)
try stats.record(dataPoint: d1)
try stats.record(dataPoint: d2)
```
The above creates a *sqlite* database with a suitable schema and records `d1` twice and `d2` once.

Here's what you see in the database after executing the above code.

```zsh
jeremyp@Magenta ~ % sqlite3 myResults.sqlite
SQLite version 3.31.1 2020-01-27 19:55:54
Enter ".help" for usage hints.
sqlite> .tables
pivot  tags 
sqlite> .schema pivot
CREATE TABLE pivot (sequenceNumber INTEGER PRIMARY KEY AUTOINCREMENT,rowName TEXT,colName TEXT,value INTEGER);
sqlite> .schema tags
CREATE TABLE tags (id INTEGER PRIMARY KEY AUTOINCREMENT,tableName TEXT,sequenceNumber INTEGER,tag TEXT);
sqlite> select * from pivot;
1|1|a|1
2|1|a|1
3|2|a|3
```
The tags table is there to allow us to tag certain results, a bit like a bookmark. For example, with 

```Swift
let stats = try Statistics<PivotData>(sqlite3File: "myResults.sqlite")
let d1 = PivotData(row: "1", col: "a", value: 1)
try stats.record(dataPoint: d1)
try stats.record(dataPoint: d1)
try stats.record(dataPoint: d1)
try stats.tag(tag: "sequence 3")
try stats.tag(sequenceNumber: 1, tag: "sequence 1")
```
I tag the most recent data point with the string "sequence 3" and the first data point with the string "sequence 1"

With a join, I can associate the tag with the relevant datapoint quite easily.

```zsh
jeremyp@Magenta ~ % sqlite3 /private/tmp/results.sqlite3
SQLite version 3.31.1 2020-01-27 19:55:54
Enter ".help" for usage hints.
sqlite> select * from tags;
1|pivot|3|Should be sequence 3
2|pivot|1|Should be sequence 1
sqlite> select p.*, t.tag from pivot p left join tags t on p.sequenceNumber = t.sequenceNumber where t.tableName = 'pivot';
1|1|a|1|Should be sequence 1
2|1|a|1|
3|1|a|1|Should be sequence 3
```
Note the `where` clause is needed in case other data sets have tags at the same sequence number.
## RollingAverage

The `RollingAverage` type is declared as a convenience to implement rolling averages and sums. Declare an instance with a given period and then add values to it. The rolling sum and average over the period are automatically calculated.